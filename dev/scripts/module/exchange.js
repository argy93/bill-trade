import $ from 'jquery';
import 'slick-carousel';

export default () => {
  $('.js-exchange-slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1000,
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<img class="is-arrow-prev" src="./static/images/chevron-left.svg" />',
    nextArrow: '<img class="is-arrow-next" src="./static/images/chevron-right.svg" />',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 3,
        },
      },
    ],
  });

  $('.is-ui-checkbox').click((e) => {
    if ($(e.currentTarget).is(':checked')) {
      $('.l-change__fast').addClass('is-active');
      $('.js-accelirated').addClass('is-active');
      $('.js-refresh').removeClass('is-active');
    } else {
      $('.l-change__fast').removeClass('is-active');
      $('.js-refresh').addClass('is-active');
      $('.js-accelirated').removeClass('is-active');
    }
  });
};
