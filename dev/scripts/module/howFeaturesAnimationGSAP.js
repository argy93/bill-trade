// eslint-disable-next-line import/no-unresolved
import TimelineMax from 'timelinemax';
// eslint-disable-next-line import/no-unresolved
import Power3 from 'power3';

export default () => {
  // First Candle
  new TimelineMax({repeat: -1})
    .fromTo('#candle-bg-1', 3,
      {y: 0, ease: Power3.easeOut},
      {y: -100, ease: Power3.easeOut})
    .fromTo('#candle-bg-1', 6,
      {y: -100, ease: Power3.easeInOut},
      {y: 100, ease: Power3.easeInOut})
    .fromTo('#candle-bg-1', 3,
      {y: 100, ease: Power3.easeIn},
      {y: 0, ease: Power3.easeIn});

  // Second Candle
  new TimelineMax({repeat: -1})
    .fromTo('#candle-bg-2', 5,
      {y: 0, ease: Power3.easeOut},
      {y: -20, ease: Power3.easeOut})
    .fromTo('#candle-bg-2', 10,
      {y: -20, ease: Power3.easeInOut},
      {y: 20, ease: Power3.easeInOut})
    .fromTo('#candle-bg-2', 5,
      {y: 20, ease: Power3.easeIn},
      {y: 0, ease: Power3.easeIn});

  // Third Candle
  new TimelineMax({repeat: -1})
    .fromTo('#candle-bg-3', 2,
      {y: 0, ease: Power3.easeOut},
      {y: 80, ease: Power3.easeOut})
    .fromTo('#candle-bg-3', 9,
      {y: 80, ease: Power3.easeInOut},
      {y: -350, ease: Power3.easeInOut})
    .fromTo('#candle-bg-3', 7,
      {y: -350, ease: Power3.easeIn},
      {y: 0, ease: Power3.easeIn});

  // Foueth Candle
  new TimelineMax({repeat: -1})
    .fromTo('#candle-bg-4', 4,
      {y: 0, ease: Power3.easeOut},
      {y: -200, ease: Power3.easeOut})
    .fromTo('#candle-bg-4', 9,
      {y: -200, ease: Power3.easeInOut},
      {y: 290, ease: Power3.easeInOut})
    .fromTo('#candle-bg-4', 5,
      {y: 290, ease: Power3.easeIn},
      {y: 0, ease: Power3.easeIn});

  // Fifth Candle
  new TimelineMax({repeat: -1, yoyo: true})
    .to('#candle-bg-5', 6, {y: -400, ease: Power3.easeInOut});

  // Sixth Candle
  new TimelineMax({repeat: -1, yoyo: true})
    .to('#candle-bg-6', 10, {y: 400, ease: Power3.easeInOut});

  // Seventh Candle
  new TimelineMax({repeat: -1, yoyo: true})
    .to('#candle-bg-7', 10, {y: -350, ease: Power3.easeInOut});

  // Eighth Candle
  new TimelineMax({repeat: -1})
    .fromTo('#candle-bg-8', 4,
      {y: 0, ease: Power3.easeOut},
      {y: -300, ease: Power3.easeOut})
    .fromTo('#candle-bg-8', 8,
      {y: -300, ease: Power3.easeInOut},
      {y: 300, ease: Power3.easeInOut})
    .fromTo('#candle-bg-8', 4,
      {y: 300, ease: Power3.easeIn},
      {y: 0, ease: Power3.easeIn});

  // Nineth Candle
  new TimelineMax({repeat: -1, yoyo: true})
    .to('#candle-bg-9', 10, {y: -350, ease: Power3.easeInOut});
};
