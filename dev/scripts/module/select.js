import $ from 'jquery';

export default () => {
  enableSelectBoxes();

  function enableSelectBoxes() {
    $('.c-select').each((item) => {
      $(item)
        .children('.c-select__checked')
        .html(
          $(item)
            .children('.c-select__container')
            .children('.c-select__option:first')
            .html(),
        );

      $(item)
        .attr('value', $(item).children('.c-select__container')
          .children('.c-select__option:first')
          .attr('value'));
    });
  }

  $('.c-select__checked, .c-select__arrow').click((event) => {
    const list = $(event.currentTarget)
      .parent()
      .children('.c-select__container');

    if (list.hasClass('is-active')) {
      list.removeClass('is-active');
    } else {
      list.addClass('is-active');
    }
  });

  $('.c-select__option').click((event) => {
    const selectOption = $(event.currentTarget);

    selectOption
      .parent()
      .removeClass('is-active');

    selectOption
      .closest('.c-select')
      .attr('value', selectOption.attr('value'));

    selectOption
      .parent()
      .siblings('.c-select__checked')
      .html(selectOption.html());
  });
};
