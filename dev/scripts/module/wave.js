// eslint-disable-next-line import/no-unresolved
import TimelineMax from 'timelinemax';
// eslint-disable-next-line import/no-unresolved
import Power3 from 'power3';

// Module description
export default () => {
  const backgroundTl = new TimelineMax({repeat: -1, yoyo: true});
  backgroundTl.to('#js-from-wave-back', 40, {morphSVG: {shape: '#js-to-wave-back', shapeIndex: 0}, ease: Power3.easeNone});
  const backgroundFrontTl = new TimelineMax({repeat: -1, yoyo: true});
  backgroundFrontTl.to('#js-from-wave-front', 40, {morphSVG: {shape: '#js-to-wave-front', shapeIndex: 0}, ease: Power3.easeNone});
};
