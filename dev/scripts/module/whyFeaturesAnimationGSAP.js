import $ from 'jquery';
// eslint-disable-next-line import/no-unresolved
import TimelineMax from 'timelinemax';
// eslint-disable-next-line import/no-unresolved
import Power0 from 'power0';
// eslint-disable-next-line import/no-unresolved
import Power3 from 'power3';
// eslint-disable-next-line import/no-unresolved
import Back from 'back';


export default () => {
  const squareTl = new TimelineMax({paused: true});
  const tradingSkillsTl = new TimelineMax({paused: true});
  const riskManagementTl = new TimelineMax({paused: true});
  const statisticTl = new TimelineMax({paused: true, repeat: -1, yoyo: true});
  const infinityTl = new TimelineMax({paused: true});
  const profitTl = new TimelineMax({paused: true});

  // Second feature (Trading Skills)
  tradingSkillsTl
    .to('#glasses', 1, {y: -10, ease: Power3.easeNone})
    .to('#smile-start', 1, {morphSVG: {shape: '#smile-end', shapeIndex: 0}}, '-=1');

  // Third feature (Trading Skills)
  riskManagementTl
    .to('#rocket-fire-start', 2, {
      morphSVG: {shape: '#rocket-fire-end', shapeIndex: 0},
      repeat: -1,
      yoyo: true,
      ease: Back.easeNone,
    })
    .to('#rocket', 3, {
      y: -20, repeat: -1, yoyo: true, ease: Power3.easeInOut,
    }, '-=3');

  // Fourth feature (Statistic)
  statisticTl
    .to('#first-statistic-graph', 2, {drawSVG: 0, ease: Power3.easeNone})
    .to('#second-statistic-graph', 3, {drawSVG: 0, ease: Power3.easeNone}, '-=2.5')
    .to('#third-statistic-graph', 2, {drawSVG: 0, ease: Power3.easeNone}, '-=2');

  // Sixth feature (Profit)
  profitTl
    .to('#present', 1, {rotation: -10, ease: Back.easeOut})
    .to('#present', 0.5, {rotation: 10, ease: Back.easeInOut})
    .to('#present', 1, {rotation: 0, ease: Back.easeInOut});

  // Five feature (infinity)
  infinityTl
    .to('#infinity', 1.5, {rotationY: -10, ease: Power0.easeNone})
    .to('#infinity', 1.5, {rotationY: 10, ease: Power0.easeNone})
    .to('#infinity', 1.5, {rotationY: 0, ease: Power0.easeNone});

  // First feature (Square)
  squareTl
    .set('.l-why__img .is-border .is-dash > div', {opacity: 1})
    .to('.is-dash__top', 0.2,
      {left: 47, linear: Power0.easeNone})
    .fromTo('.is-dash__right', 0.2,
      {top: 11, linear: Power0.easeNone},
      {top: 49, linear: Power0.easeNone})
    .to('.is-dash__bottom', 0.2,
      {right: 47, linear: Power0.easeNone})
    .fromTo('.is-dash__left', 0.2,
      {bottom: 8, linear: Power0.easeNone},
      {bottom: 47, linear: Power0.easeNone});

  // ----- Animation condition Handlers -----

  // First
  $('#l-why__square').hover(() => {
    squareTl.pause(0);
    squareTl.play();
  }, () => {});

  // Second
  $('#l-why__skills').hover(
    () => tradingSkillsTl.play(),
    () => tradingSkillsTl.pause(0),
  );

  // Third
  $('#l-why__risk').hover(
    () => riskManagementTl.play(),
    () => riskManagementTl.pause(0),
  );

  // Fourth
  $('#l-why__statistic').hover(
    () => statisticTl.play(),
    () => statisticTl.pause(0),
  );

  // Five
  $('#l-why__infinity').hover(
    () => {
      infinityTl.pause(0);
      infinityTl.play();
    },
    () => infinityTl.pause(0),
  );

  // Sixth
  $('#l-why__present').hover(
    () => {
      profitTl.pause();
      profitTl.play();
    },
    () => profitTl.pause(0),
  );
};
