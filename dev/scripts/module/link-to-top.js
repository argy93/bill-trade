import $ from 'jquery';

export default () => {
  const pageHeight = $('body').outerHeight();
  const topLinkBlock = (pageHeight / 100) * 15;

  $(window).scroll(() => {
    if ($(window).scrollTop() > topLinkBlock) {
      $('.c-toplink').fadeIn();
    } else {
      $('.c-toplink').fadeOut();
    }
  });

  $('.c-toplink').click(() => {
    $('body,html').animate({scrollTop: 0}, 1300);
  });
};
