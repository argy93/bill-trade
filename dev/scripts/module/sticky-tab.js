import $ from 'jquery';
// Module description
const defaultValues = {
  // Количество пикселей необходимых для переклчения таба
  intensity: 150,
  // Событие скролла, передает текущий процент скролла
  // eslint-disable-next-line no-unused-vars
  onScroll(percent) {},
  // Событие шага, испускается во время необходимости переключения на конкретный айтем
  // eslint-disable-next-line no-unused-vars
  onStep(step) {},
};

// eslint-disable-next-line consistent-return
export default ($dom, $tabCount, conf) => {
  if ($(window).innerWidth() >= 768) {
    $.extend(defaultValues, conf);
    // eslint-disable-next-line no-param-reassign
    conf = defaultValues;

    // eslint-disable-next-line no-unused-vars
    const step = 1;
    const $wrapper = $dom.closest('.js-sticky-tab-wrap');
    const main_height = $dom.outerHeight();
    const wrapper_height = main_height + conf.intensity * ($tabCount - 1);
    const start_point = $dom.offset().top;
    const end_point = wrapper_height - window.outerHeight;
    const delimiter = end_point / 100;
    const zone = 100 / $tabCount;

    $wrapper.css({height: wrapper_height});

    $(window).on('scroll', onScroll);

    /**
     * Обработчик сролла
     */
    // eslint-disable-next-line no-inner-declarations
    function onScroll() {
      const position = window.scrollY - start_point;
      const percent = position / delimiter;

      // console.log(percent);

      if (position > 0 && position < end_point) {
        conf.onScroll(percent);
        conf.onStep(Math.ceil(percent / zone));
      } else if (position < 0) {
        conf.onScroll(0);
        conf.onStep(1);
      } else if (position > end_point) {
        conf.onScroll(100);
        conf.onStep($tabCount);
      }
    }

    /**
     * Метод - скроллит до выбраной зоны
     * @param zone_number - номер зоны
     */
    // eslint-disable-next-line no-inner-declarations
    function scrollToZone(zone_number) {
      const percent = zone_number * (zone - zone / 2);
      const offset = delimiter * percent;

      if (zone_number === 1) {
        animate(start_point);
      } else if (zone_number === $tabCount) {
        animate(start_point + end_point);
      } else {
        animate(start_point + offset);
      }

      function animate(to) {
        $('html, body').animate({scrollTop: to}, 600);
      }
    }

    return {
      scrollToZone,
    };
  }
};
