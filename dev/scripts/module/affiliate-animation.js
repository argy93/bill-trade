import $ from 'jquery';
// eslint-disable-next-line import/no-unresolved
import TweenMax from 'tweenmax';
// eslint-disable-next-line import/no-unresolved
import TimelineMax from 'timelinemax';
// eslint-disable-next-line import/no-unresolved
import Power3 from 'power3';
// eslint-disable-next-line import/no-unresolved
import Back from 'back';

export default () => {
  /*
      ----- Vars -----
  */
  let init = false;
  const platform = $('#platform');

  // Investors
  const trader = $('#trader');
  const firstInvestor = $('#first-investor');
  const secondInvestor = $('#second-investor');
  const thirdInvestor = $('#third-investor');

  // Users
  const mainUser = $('#main-user');

  // Users messages
  const mainUserMessageLeft = $('#main-user-message-left');
  const mainUserMessageRight = $('#main-user-message-right');
  const firstInvestorMessage = $('#first-investor-message');
  const secondInvestorMessage = $('#second-investor-message');

  // Arrows
  const mainArrowsLeft = $('#main-arrows-left');
  const mainArrowsRight = $('#main-arrows-right');
  const firstInvestorArrow = $('#first-investor-arrow');
  const secondInvestorArrow = $('#second-investor-arrow');

  // Profit
  const traderProfit = $('#trader-profit');
  const firstInvestorProfit = $('#first-investor-profit');
  const secondInvestorProfit = $('#second-investor-profit');
  const thirdInvestorProfit = $('#third-investor-profit');

  // Profit Arrows
  const traderProfitArrow = $('#trader-profit-arrow');
  const firstInvestorProfitArrow = $('#first-investor-profit-arrow');
  const secondInvestorProfitArrow = $('#second-investor-profit-arrow');
  const thirdInvestorProfitArrow = $('#third-investor-profit-arrow');


  /*
      ----- Start Settings -----
  */
  const platformConfig = {y: 100, opacity: 0};
  const bounceConfig = {scale: 0.5, opacity: 0, y: 25};

  TweenMax.set(platform, platformConfig);
  TweenMax.set(mainUser, bounceConfig);
  TweenMax.set(mainUserMessageLeft, {opacity: 0, x: 45, y: -10});
  TweenMax.set(mainUserMessageRight, {opacity: 0, x: -45, y: -10});
  TweenMax.set(mainArrowsRight, {opacity: 0, x: -10, y: -5});
  TweenMax.set(mainArrowsLeft, {opacity: 0, x: 10, y: -5});
  TweenMax.set(trader, bounceConfig);
  TweenMax.set(firstInvestor, bounceConfig);
  TweenMax.set(traderProfit, bounceConfig);
  TweenMax.set(firstInvestorProfit, bounceConfig);
  TweenMax.set(traderProfitArrow, {opacity: 0, x: -10, y: 5});
  TweenMax.set(firstInvestorProfitArrow, {opacity: 0, x: 10, y: 5});
  TweenMax.set(firstInvestorMessage, {opacity: 0, x: 45, y: -10});
  TweenMax.set(firstInvestorArrow, {opacity: 0, x: 5, y: -10});
  TweenMax.set(secondInvestor, bounceConfig);
  TweenMax.set(secondInvestorProfit, bounceConfig);
  TweenMax.set(secondInvestorProfitArrow, {opacity: 0, x: -5, y: 15});
  TweenMax.set(secondInvestorMessage, {opacity: 0, x: -5, y: -15});
  TweenMax.set(secondInvestorArrow, {opacity: 0, x: -10});
  TweenMax.set(thirdInvestor, bounceConfig);
  TweenMax.set(thirdInvestorProfit, bounceConfig);
  TweenMax.set(thirdInvestorProfitArrow, {opacity: 0, x: 10, y: 5});

  /*
      ----- TimeLine Functions -----
  */
  const tl = new TimelineMax({
    paused: true,
  });

  tl.to(platform, 2, {y: 0, opacity: 1, ease: Power3.easeOut});
  tl.to(mainUser, 0.5, {
    y: 0, opacity: 1, scale: 1, ease: Back.easeOut.config(1.5),
  }, '-=1.5');
  tl.add([
    TweenMax.to(mainUserMessageLeft, 0.5, {
      x: 0, y: 0, opacity: 1, rotation: 0, ease: Power3.easeOut,
    }),
    TweenMax.to(mainUserMessageRight, 0.5, {
      x: 0, y: 0, opacity: 1, rotation: 0, ease: Power3.easeOut,
    }),
  ]);
  tl.add([
    TweenMax.to(mainArrowsLeft, 0.5, {
      x: 0, y: 0, opacity: 1, ease: Power3.easeOut,
    }),
    TweenMax.to(mainArrowsRight, 0.5, {
      x: 0, y: 0, opacity: 1, ease: Power3.easeOut,
    }),
  ]);
  tl.add([
    TweenMax.to(trader, 0.5, {
      y: 0, opacity: 1, scale: 1, ease: Back.easeOut.config(1.5),
    }),
    TweenMax.to(firstInvestor, 0.5, {
      y: 0, opacity: 1, scale: 1, ease: Back.easeOut.config(1.5),
    }),
  ]);
  tl.add([
    TweenMax.to(traderProfit, 0.5, {
      y: 0, opacity: 1, scale: 1, ease: Back.easeOut.config(1.5),
    }),
    TweenMax.to(firstInvestorProfit, 0.5, {
      y: 0, opacity: 1, scale: 1, ease: Back.easeOut.config(1.5),
    }),
  ]);
  tl.add([
    TweenMax.to(traderProfitArrow, 0.5, {
      x: 0, y: 0, opacity: 1, ease: Power3.easeOut,
    }),
    TweenMax.to(firstInvestorProfitArrow, 0.5, {
      x: 0, y: 0, opacity: 1, ease: Power3.easeOut,
    }),
  ], '+=0.5');
  tl.to(firstInvestorMessage, 0.5, {
    x: 0, y: 0, opacity: 1, ease: Power3.easeOut,
  });
  tl.to(firstInvestorArrow, 0.5, {
    x: 0, y: 0, opacity: 1, ease: Power3.easeOut,
  });
  tl.to(secondInvestor, 0.5, {
    y: 0, opacity: 1, scale: 1, ease: Back.easeOut.config(1.5),
  });
  tl.to(secondInvestorProfit, 0.5, {
    y: 0, opacity: 1, scale: 1, ease: Back.easeOut.config(1.5),
  });
  tl.add([
    TweenMax.to(secondInvestorProfitArrow, 0.5, {
      x: 0, y: 0, opacity: 1, ease: Power3.easeOut,
    }),
    TweenMax.to(secondInvestorMessage, 0.5, {
      x: 0, y: 0, opacity: 1, ease: Power3.easeOut,
    }),
  ]);
  tl.to(secondInvestorArrow, 0.5, {x: 0, opacity: 1, ease: Power3.easeOut});
  tl.to(thirdInvestor, 0.5, {
    y: 0, opacity: 1, scale: 1, ease: Back.easeOut.config(1.5),
  });
  tl.to(thirdInvestorProfit, 0.5, {
    y: 0, opacity: 1, scale: 1, ease: Back.easeOut.config(1.5),
  });
  tl.to(thirdInvestorProfitArrow, 0.5, {
    x: 0, y: 0, opacity: 1, ease: Power3.easeOut,
  });

  $(window).scroll(() => {
    if ($('.js-affiliate').hasClass('aos-animate')) {
      if (init) return;

      tl.play();
      init = true;
    }
  });
};
