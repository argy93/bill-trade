import $ from 'jquery';

export default () => {
  if ($(window).innerWidth() >= 600) {
    const parallaxItems = [];
    const defaultSpeed = 1.2;

    init(parallaxItems, defaultSpeed);

    $(window).scroll(() => {
      const currentTop = window.scrollY;

      parallaxItems.forEach((item) => {
        if (currentTop > item.start && currentTop < item.end) {
          const nextPosition = -(((currentTop - item.start) * item.speed));

          item.$el.css('transform', `translate3d(0, ${nextPosition}px, 0)`);
        }
      });
    });
  }
};

const init = (parallaxItems, defaultSpeed) => {
  $('.js-parallax').each((index, el) => {
    const $el = $(el);
    const item = {
      $el,
      el,
      offsetTop: $el.offset().top,
      height: $el.outerHeight(),
      speed: $el.data('speed') ? $el.data('speed') : defaultSpeed,
      start: $el.offset().top - window.innerHeight,
      end: $el.offset().top + $el.outerHeight(),
    };

    item.pathLength = (item.end - item.start) * item.speed;

    parallaxItems.push(item);
  });
};
