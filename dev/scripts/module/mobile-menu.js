import $ from 'jquery';

export default () => {
  if ($(window).innerWidth() <= 991) {
    const $modalContainer = $('.l-modal-container');

    $('.c-menu__hamburger').click(() => {
      $modalContainer.addClass('is-active');
      $modalContainer.children('.c-modal').addClass('is-active');
    });

    $('.l-modal-container__close').click(() => {
      $modalContainer.removeClass('is-active');
      $modalContainer.children('.c-modal').removeClass('is-active');
    });
  }
};
