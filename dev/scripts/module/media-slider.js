import $ from 'jquery';
import 'slick-carousel';

export default () => {
  $('.js-media-slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1000,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
  });
};
