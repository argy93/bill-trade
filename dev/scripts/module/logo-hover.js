import $ from 'jquery';

export default () => {
  $('.is-logo').hover(
    () => {
      $('#is-logo__red').css('fill', '#ff0000');
      $('#is-logo__green').css('fill', '#008000');
    },
    () => {
      $('#is-logo__red').css('fill', '#1F2123');
      $('#is-logo__green').css('fill', '#1F2123');
    },
  );
};
