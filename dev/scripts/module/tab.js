/* eslint-disable func-names */
import $ from 'jquery';

export default () => {
  const selector = $('.js-tab');

  selector.each((iter, dom) => {
    const el = $(dom);

    selector.find('[data-tab]').click((event) => {
      setActive(el, event.currentTarget);
    });
  });

  function setActiveTab($tab, number) {
    setActive($tab, $tab.find('[data-tab]').eq(number - 1));
  }

  function setActive(targetElement, currentTarget) {
    const target = $(currentTarget).attr('data-tab');

    targetElement.find('.js-navtab').removeClass('is-active');
    $(currentTarget).addClass('is-active');

    targetElement.find('.js-content').removeClass('is-active');
    targetElement.find(`.js-content${target}`).addClass('is-active');
  }

  return { setActiveTab };
};
