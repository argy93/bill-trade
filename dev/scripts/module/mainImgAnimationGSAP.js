import $ from 'jquery';
// eslint-disable-next-line import/no-unresolved
import TweenMax from 'tweenmax';
// eslint-disable-next-line import/no-unresolved
import TimelineMax from 'timelinemax';
// eslint-disable-next-line import/no-unresolved
import Power3 from 'power3';

export default () => {
  const duration = 10;
  const candle1 = $('#candle-1');
  const candle2 = $('#candle-2');
  const candle3 = $('#candle-3');
  const candle4 = $('#candle-4');
  const candle5 = $('#candle-5');
  const candle6 = $('#candle-6');

  TweenMax.set(candle1, {y: -35});
  TweenMax.set(candle2, {y: 20});
  TweenMax.set(candle4, {y: 10});

  new TimelineMax({repeat: -1, yoyo: true})
    .fromTo(candle1, duration, {y: -35, ease: Power3.easeNone}, {y: 25, ease: Power3.easeNone})
    .fromTo(candle2, duration, {y: 20, ease: Power3.easeNone}, {y: -10, ease: Power3.easeNone}, '-=10')
    .fromTo(candle3, duration, {y: 0, ease: Power3.easeNone}, {y: 20, ease: Power3.easeNone}, '-=10')
    .fromTo(candle4, duration, {y: 10, ease: Power3.easeNone}, {y: -25, ease: Power3.easeNone}, '-=10')
    .fromTo(candle5, duration, {y: 0, ease: Power3.easeNone}, {y: 45, ease: Power3.easeNone}, '-=10')
    .fromTo(candle6, duration, {y: 0, ease: Power3.easeNone}, {y: 25, ease: Power3.easeNone}, '-=10');
};
