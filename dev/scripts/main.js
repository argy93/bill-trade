// eslint-disable-next-line import/no-extraneous-dependencies
import 'aos/dist/aos.css';
import '../style/main.scss';
import $ from 'jquery';
// eslint-disable-next-line import/no-extraneous-dependencies
import AOS from 'aos';
import Tab from './module/tab';
import media_slider from './module/media-slider';
import exchange from './module/exchange';
import stickyTab from './module/sticky-tab';
import mainImgAnimation from './module/mainImgAnimationGSAP';
import whyFeaturesAnimation from './module/whyFeaturesAnimationGSAP';
import howFeaturesAnimation from './module/howFeaturesAnimationGSAP';
import mobile_menu from './module/mobile-menu';
import parallax from './module/parallax';
import affiliate from './module/affiliate-animation';
import logoHover from './module/logo-hover';
import wave from './module/wave';
import linkToTop from './module/link-to-top';
import select from './module/select';

$(document).ready(() => {
  mobile_menu();
  media_slider();
  exchange();
  mainImgAnimation();
  whyFeaturesAnimation();
  howFeaturesAnimation();
  wave();
  linkToTop();

  const tab = Tab();

  const $sticky_tab = $('.js-sticky-tab');
  const $sticky_tab_count = $sticky_tab.find('.js-content').length;

  const sticky_tab = stickyTab($sticky_tab, $sticky_tab_count, {
    onScroll: (percent) => {
      $sticky_tab.find('.js-sticky-tab-bar').css('width', `${percent}%`);
    },
    onStep: (step) => {
      tab.setActiveTab($sticky_tab.find('.js-tab'), step);
    },
  });

  $sticky_tab.find($sticky_tab.find('.js-tab [data-tab]')).click((event) => {
    const index = $(event.currentTarget).prevAll().length + 1;
    sticky_tab.scrollToZone(index);
  });

  parallax();
  affiliate();
  logoHover();
  select();
  AOS.init();
});
